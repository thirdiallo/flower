import { Commande, CommandeDto } from './commande';
import { State, StateDto } from './state';

export class Address {
    id: number;
    reference: string;
    quartier: string;
    phone: string;
    city: string;
    rue: string;

    state: State;

    commande: Commande;

}

export class AddressDto {
    id: number;
    reference: string;
    quartier: string;
    phone: string;
    city: string;
    rue: string;

    stateDto: StateDto;

    commandeDto: CommandeDto;

}
