import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { TokenStorageService } from './../auth/token-storage.service';
import { ProductDto } from './../models/product';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CatalogueService {

  apiServerUrl = environment.apiBaseUrl;

  id: any;
  currentUser: any;
  username: any;

  public Currency = { name: 'Dollar', currency: 'USD', price: 1 } // Default Currency
  public OpenCart: boolean = false;
  public Products

  apiBaseUrl: 'http://localhost:8081/casa-solaire/v1';

  constructor(private http: HttpClient,
            private tokenService: TokenStorageService
  ) {
  }

  public getListProductDtoBySelectedIsTrue(): Observable<ProductDto[]> {
    return this.http.get<ProductDto[]>(`${this.apiServerUrl}/products/searchProductBySelectedIsTrue`);
  }

  public getTop24ProductDtoOrderByCreatedDateDesc(): Observable<ProductDto[]> {
    return this.http.get<ProductDto[]>(`${this.apiServerUrl}/products/searchTop24ProductByOrder`);
  }

  public getListProductDtoByCategoryId(scatId: number): Observable<ProductDto[]> {
    return this.http.get<ProductDto[]>(`${this.apiServerUrl}/products/productsBySubCategories/${scatId}`);
  }

  public getListProductDtoByPageable(page: number, size: number): Observable<ProductDto[]> {
    return this.http.get<ProductDto[]>(`${this.apiServerUrl}/products/searchProductByPageables?page=`+page+"&size="+size);
  }

  public getListProductDtoByKeyword(keyword: string): Observable<ProductDto[]> {
    return this.http.get<ProductDto[]>(`${this.apiServerUrl}/products/searchProductByKeyword?keyword=`+keyword);
  }

  public getListProductDtoBySamePrice(price: number): Observable<ProductDto[]> {
    return this.http.get<ProductDto[]>(`${this.apiServerUrl}/products/searchProductByPrice/${price}`);
  }

  public getListProductDtoByPricemMinMax(min: number, max: number): Observable<ProductDto[]> {
    return this.http.get<ProductDto[]>(`${this.apiServerUrl}/products/searchAllProductByPriceMinMax/${min}/${max}`);
  }

  public getListProductDtoByScategoryByPageable(scatId: number, page: number, size: number): Observable<ProductDto[]> {
    const searchUrl = (this.apiServerUrl+"/products/searchProductBySubcategoryByPageables?id="+scatId+"&page="+page+"&size="+size);
    console.log("Search Url---", searchUrl);
    return this.http.get<ProductDto[]>(searchUrl);
  }

  public getListProductDtoBySamePriceByPageable(price: number, page: number, size: number): Observable<ProductDto[]> {
    const searchbyPriceUrl = (this.apiServerUrl+"/products/searchProductBySamePriceByPageables?price="+price+"&page="+page+"&size="+size);
    console.log("Search Price Url---", searchbyPriceUrl);
    return this.http.get<ProductDto[]>(searchbyPriceUrl);
  }

  public countNumberOfProductInSubCategory(sucatId: number): Observable<ProductDto> {
    return this.http.get<ProductDto>(`${this.apiServerUrl}/products/countNumberOfProductInSubCat/${sucatId}`);
  }

  public getPhotoProduct() {
    return this.http.get(`${this.apiServerUrl}/products/photoProduct`);
  }

  getCurrentUser(): Observable<any> {
    return this.tokenService.getUser();
  }

  getLogginUser() {
    const user = this.tokenService.getLogginUser();
    this.currentUser = user;
  }

  getUsername() {
    const user = this.tokenService.getUser();
    this.username = user.username;
  }


  getUserId() {
    const user = this.tokenService.getUser();
    this.id = user.id;
  }
}
