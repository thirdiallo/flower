import { LigneCommande } from './ligne-commande';
import { Address, AddressDto } from './address';
import { Utilisateur, UtilisateurDto } from './utilisateur';
import { Client, ClientDto } from './client';

export class Commande {
    id: number;
    numeroOrder: number;
    totalQuantity: number;
    totalPrice: number;
    total: number;
    orderDate: Date;
    status: string;
    sessionId: string;
    orderTrackingNumber: string;

    client: Client;

    utilisateur: Utilisateur;

    shippingAddress: Address;

    billingAddress: Address;

    orderItemList :Array<LigneCommande>=[];

}

export class CommandeDto {
    id: number;
    numeroOrder: number;
    totalQuantity: number;
    totalPrice: number;
    total: number;
    orderDate: Date;
    status: string;
    sessionId: string;
    orderTrackingNumber: string;

    clientDto: ClientDto;

    utilisateurDto: UtilisateurDto;

    shippingAddressDto: AddressDto;

    billingAddressDto: AddressDto;

    orderItemList :Array<LigneCommande>=[];

}
