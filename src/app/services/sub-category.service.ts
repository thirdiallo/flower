import { environment } from './../../environments/environment';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { SubCategoryDto } from '../models/sub-category';

@Injectable({
  providedIn: 'root'
})
export class SubCategoryService {

  apiServerUrl = environment.apiBaseUrl;

  //apiServerUrl = "https://businesse-admin.herokuapp.com/shop-mania/v1";

//  private apiServerUrl = "http://localhost:8081/shop-mania/v1/";

  choixmenu : string  = 'A';

  dataForm:  FormGroup;

  listData : SubCategoryDto[];

  formData:  SubCategoryDto;

  constructor(private http: HttpClient) {
  }

  /********************* SubCategoryDto ******************/

  public getSubCategoryDtos(): Observable<SubCategoryDto[]> {
    return this.http.get<SubCategoryDto[]>(`${this.apiServerUrl}/subcategories/all`);
  }

  public getALLSuCategoryDtosOrderByIdDesc(): Observable<SubCategoryDto[]> {
    return this.http.get<SubCategoryDto[]>(`${this.apiServerUrl}/subcategories/searchAllSubCategoriesOrderByIdDesc`);
  }

  public getSubCategoryDtoById(subCatId: number): Observable<SubCategoryDto> {
    return this.http.get<SubCategoryDto>(`${this.apiServerUrl}/subcategories/findById/${subCatId}`);
  }

  public addSubCategoryDto(subCat: SubCategoryDto): Observable<SubCategoryDto> {
    return this.http.post<SubCategoryDto>(`${this.apiServerUrl}/subcategories/create`, subCat);
  }

  public updateSubCategoryDto(subCatId: number, subCat: SubCategoryDto): Observable<SubCategoryDto> {
    return this.http.put<SubCategoryDto>(`${this.apiServerUrl}/subcategories/update/${subCatId}`, subCat);
  }

  public deleteSubCategoryDto(subCatId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/subcategories/delete/${subCatId}`);
  }

}
