import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { TokenStorageService } from './../auth/token-storage.service';
import { ProductService } from './../shared/services/product.service';
import { RatingDto } from './../models/rating';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RatingService {

  apiServerUrl = environment.apiBaseUrl;


  //apiServerUrl = "https://businesse-admin.herokuapp.com/shop-mania/v1";

  id: any;
  prodId: any;

  constructor(private http: HttpClient,
              private tokenService: TokenStorageService
  ) {
  }

  /***************************** RatingDto */

  public getRatingDtos(): Observable<RatingDto[]> {
    return this.http.get<RatingDto[]>(`${this.apiServerUrl}/ratings/all`);
  }

  public getAllRatingDtosOrderByIdDesc(): Observable<RatingDto[]> {
    return this.http.get<RatingDto[]>(`${this.apiServerUrl}/ratings/searchAllRatingsOrderByIdDesc`);
  }

  public getTop3RatingOrderByCreatedDateDesc(): Observable<RatingDto[]> {
    return this.http.get<RatingDto[]>(`${this.apiServerUrl}/ratings/searchTop3RatingOrderByCreatedDateDesc`);
  }

  public getTop4RatingOrderByCreatedDateDescByProduct(ratId: string): Observable<RatingDto[]> {
    return this.http.get<RatingDto[]>(`${this.apiServerUrl}/ratings/searchTop4RatingOrderByCreatedDateDescByProductId/${ratId}`);
  }


  public getRatingDtoById(ratId: number): Observable<RatingDto> {
    return this.http.get<RatingDto>(`${this.apiServerUrl}/ratings/findById/${ratId}`);
  }

  public addRatingDto(ratingDto: RatingDto): Observable<RatingDto> {
    return this.http.post<RatingDto>(`${this.apiServerUrl}/ratings/create`, ratingDto);
  }

  public addRatingToArticle(ratingDto: RatingDto, reference: string, userId:number): Observable<RatingDto> {
    return this.http.post<RatingDto>(`${this.apiServerUrl}/ratings/createRatingToArticle?reference=${reference}&userId=${userId}`, ratingDto);
  }

  public deleteRatingDto(ratId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/ratings/delete/${ratId}`);
  }

  getUserId() {
    const user = this.tokenService.getUser();
    this.id = user.id
  }


}
