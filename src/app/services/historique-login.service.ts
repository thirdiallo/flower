import { environment } from './../../environments/environment';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { HistoriqueLoginDto } from './../models/historique-login';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HistoriqueLoginService {
  
  apiServerUrl = environment.apiBaseUrl;

   // apiServerUrl = "https://businesse-admin.herokuapp.com/shop-mania/v1";


  choixmenu : string  = 'A';

  dataForm:  FormGroup;

  listData : HistoriqueLoginDto[];

  formData:  HistoriqueLoginDto;

  constructor(private http: HttpClient) {
  }

  /********************* HistoriqueLoginDto ******************/

  public getHistoriqueLoginDtos(): Observable<HistoriqueLoginDto[]> {
    return this.http.get<HistoriqueLoginDto[]>(`${this.apiServerUrl}/historiqueLogins/all`);
  }

  public getALLHistoriqueLoginDtosOrderByIdDesc(): Observable<HistoriqueLoginDto[]> {
    return this.http.get<HistoriqueLoginDto[]>(`${this.apiServerUrl}/historiqueLogins/searchAllHistoriqueLoginsOrderByIdDesc`);
  }

  public getHistoriqueLoginDtoById(histLogId: number): Observable<HistoriqueLoginDto> {
    return this.http.get<HistoriqueLoginDto>(`${this.apiServerUrl}/historiqueLogins/findById/${histLogId}`);
  }

  public addHistoriqueLoginDto(HistoriqueLoginDto: HistoriqueLoginDto): Observable<HistoriqueLoginDto> {
    return this.http.post<HistoriqueLoginDto>(`${this.apiServerUrl}/historiqueLogins/create`, HistoriqueLoginDto);
  }

  public updateHistoriqueLoginDto(histLogId: number, HistoriqueLoginDto: HistoriqueLoginDto): Observable<HistoriqueLoginDto> {
    return this.http.put<HistoriqueLoginDto>(`${this.apiServerUrl}/historiqueLogins/update/${histLogId}`, HistoriqueLoginDto);
  }

  public deleteHistoriqueLoginDto(histLogId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/historiqueLogins/delete/${histLogId}`);
  }
  
}
