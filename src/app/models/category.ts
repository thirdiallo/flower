export class Category {
    id: number;
    categoryName: string;
    description: string;

}

export class CategoryDto {
    id: number;
    categoryName: string;
    description: string;

}
