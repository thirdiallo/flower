import { environment } from './../../environments/environment';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { WishlistDto } from './../models/wishlist';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WishlistService {

  apiServerUrl = environment.apiBaseUrl;

  choixmenu : string  = 'A';

  listData : WishlistDto[];

  formData:  WishlistDto;

  dataForm:  FormGroup;

  constructor(private http: HttpClient) {
  }

  /***************************** WishlistDto    *************/

  public getWishlistDtos(): Observable<WishlistDto[]> {
    return this.http.get<WishlistDto[]>(`${this.apiServerUrl}/wishlists/all`);
  }

  public getAllWishlistDtosOrderByIdDesc(): Observable<WishlistDto[]> {
    return this.http.get<WishlistDto[]>(`${this.apiServerUrl}/wishlists/searchAllwishlistsOrderByIdDesc`);
  }

  public getWishlistDtoById(statId: number): Observable<WishlistDto> {
    return this.http.get<WishlistDto>(`${this.apiServerUrl}/wishlists/findById/${statId}`);
  }

  public getWishlistDtoByDesignation(designation: string): Observable<WishlistDto> {
    return this.http.get<WishlistDto>(`${this.apiServerUrl}/wishlists/${designation}`);
  }

  public addWishlistDto(WishlistDto: WishlistDto): Observable<WishlistDto> {
    return this.http.post<WishlistDto>(`${this.apiServerUrl}/wishlists/create`, WishlistDto);
  }

  public updateWishlistDto(statId: number, wishlistDto: WishlistDto): Observable<WishlistDto> {
    return this.http.put<WishlistDto>(`${this.apiServerUrl}/wishlists/update/${statId}`, wishlistDto);
  }

  public deleteWishlistDto(statId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/wishlists/delete/${statId}`);
  }
}
