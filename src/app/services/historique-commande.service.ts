import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { FormGroup } from '@angular/forms';
import { HistoriqueCommandeDto } from './../models/historique-commande';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HistoriqueCommandeService {

  apiServerUrl = environment.apiBaseUrl;

   // apiServerUrl = "https://businesse-admin.herokuapp.com/shop-mania/v1";


  choixmenu : string  = 'A';

  dataForm:  FormGroup;

  listData : HistoriqueCommandeDto[];

  formData:  HistoriqueCommandeDto;

  constructor(private http: HttpClient) {
  }

  /********************* HistoriqueCommandeDto ******************/

  public getHistoriqueCommandeDtos(): Observable<HistoriqueCommandeDto[]> {
    return this.http.get<HistoriqueCommandeDto[]>(`${this.apiServerUrl}/historiqueCommandes/all`);
  }

  public getALLHistoriqueCommandeDtosOrderByIdDesc(): Observable<HistoriqueCommandeDto[]> {
    return this.http.get<HistoriqueCommandeDto[]>(`${this.apiServerUrl}/historiqueCommandes/searchAllhistoriqueCommandesOrderByIdDesc`);
  }

  public getHistoriqueCommandeDtoById(histLogId: number): Observable<HistoriqueCommandeDto> {
    return this.http.get<HistoriqueCommandeDto>(`${this.apiServerUrl}/historiqueCommandes/findById/${histLogId}`);
  }

  public addHistoriqueCommandeDto(HistoriqueCommandeDto: HistoriqueCommandeDto): Observable<HistoriqueCommandeDto> {
    return this.http.post<HistoriqueCommandeDto>(`${this.apiServerUrl}/historiqueCommandes/create`, HistoriqueCommandeDto);
  }

  public updateHistoriqueCommandeDto(histLogId: number, HistoriqueCommandeDto: HistoriqueCommandeDto): Observable<HistoriqueCommandeDto> {
    return this.http.put<HistoriqueCommandeDto>(`${this.apiServerUrl}/historiqueCommandes/update/${histLogId}`, HistoriqueCommandeDto);
  }

  public deleteHistoriqueCommandeDto(histLogId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/historiqueCommandes/delete/${histLogId}`);
  }
}
