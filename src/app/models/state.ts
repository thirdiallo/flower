import { Country, CountryDto } from './country';

export class State {
    id: number;
    code: string;
    name: string;

    country: Country;
}

export class StateDto {
    id: number;
    code: string;
    name: string;

    countryDto: CountryDto;
}
