import { SubCategory, SubCategoryDto } from './sub-category';

export class Product {
    id: number;
    reference: string;
    designation: string;
    quantity: number;
    quantite: number;
    price: number;
    currentPrice: number;
    isPromo: boolean;
    isSelected: boolean;
    isInstock: boolean;
    description: string;
    manufactured: string;
    imageUrl: string;
    createdDate: Date;
    lastUpDated: Date;

    subcategory: SubCategory;
}

export class ProductDto {
    id: number;
    reference: string;
    designation: string;
    quantity: number;
    quantite: number;
    price: number;
    currentPrice: number;
    isPromo: boolean;
    isSelected: boolean;
    isInstock: boolean;
    description: string;
    manufactured: string;
    imageUrl: string;
    createdDate: Date;
    lastUpDated: Date;

    subcategoryDto: SubCategoryDto;
}

