import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpRequest, HttpClient } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { ProductDto } from './../models/product';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  apiServerUrl = environment.apiBaseUrl;

  public Currency = { name: 'Dollar', currency: 'USD', price: 1 } // Default Currency
  public OpenCart: boolean = false;
  public Products

  //apiServerUrl = "https://businesse-admin.herokuapp.com/shop-mania/v1";

  choixmenu : string  = 'A';
  listData : ProductDto[];

  formData:  FormGroup;

  constructor(private http: HttpClient,
              private toastrService: ToastrService) {
  } 

  /************   ProductDto  ***************/

  public getProductDTOs(): Observable<ProductDto[]> {
    return this.http.get<ProductDto[]>(`${this.apiServerUrl}/products/all`);
  }

  public getProductDTOsOrderByIdDesc(): Observable<ProductDto[]> {
    return this.http.get<ProductDto[]>(`${this.apiServerUrl}/products/searchAllProductOrderByIdDesc`);
  }

  public getProductDTOById(prodId: number): Observable<ProductDto> {
    return this.http.get<ProductDto>(`${this.apiServerUrl}/products/findById/${prodId}`);
  }

  public getProductDTOByReference(reference: string): Observable<ProductDto> {
    return this.http.get<ProductDto>(`${this.apiServerUrl}/products/searchProductbyReference/${reference}`);
  }

  public addProductDTO(prodDTO: ProductDto): Observable<ProductDto> {
    return this.http.post<ProductDto>(`${this.apiServerUrl}/products/create`, prodDTO);
  }

  public addProductDTOWithPhoto(formData: FormData): Observable<any> {
    const req = new HttpRequest('POST', `${this.apiServerUrl}/products/createWithFile`, formData, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(req);
  }

  public addProductDTOWithPhotoInFolder(formData: FormData): Observable<any> {
    const req = new HttpRequest('POST', `${this.apiServerUrl}/products/createWithFileInFolder`, formData, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(req);
  }


  public updateProductDTO(prodId: number, prodDTO: ProductDto): Observable<ProductDto> {
    return this.http.put<ProductDto>(`${this.apiServerUrl}/products/update/${prodId}`, prodDTO);
  }

  uploadPhotoProductDTO(file: File, id: number): Observable<HttpEvent<{}>> {
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    const req = new HttpRequest('POST', this.apiServerUrl+'/products/uploadProductPhoto/' + id, formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }

  uploadPhotoProductDTOInFolder(file: File, id: number): Observable<HttpEvent<{}>> {
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    const req = new HttpRequest('POST', this.apiServerUrl+'/products/uploadArticlePhotoInFolder/' + id, formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }

  public getPhotoProduct() {
    return this.http.get(`${this.apiServerUrl}/products/photoProduct`);
  }

  public deleteProductDTO(prodId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/products/delete/${prodId}`);
  }


  incrementQuantityProductDTO(prodDTO: ProductDto) {
    prodDTO.quantite++;

  }

  decrementQuantityProductDTO(prodDTO: ProductDto) {
    prodDTO.quantite--;

  }
}
