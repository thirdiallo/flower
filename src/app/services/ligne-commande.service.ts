import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { LigneCommandeDto } from './../models/ligne-commande';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LigneCommandeService {

  apiServerUrl = environment.apiBaseUrl;


  //apiServerUrl = "https://businesse-admin.herokuapp.com/shop-mania/v1";

  listData : LigneCommandeDto[];

  constructor(private http: HttpClient) {
  }

  /*********************** LigneCommandeDTO */

  public getLigneCommandeDtos(): Observable<LigneCommandeDto[]> {
    return this.http.get<LigneCommandeDto[]>(`${this.apiServerUrl}/lignecommandes/all`);
  }

  public getAllLigneCommandeDtosOrderByIdDesc(): Observable<LigneCommandeDto[]> {
    return this.http.get<LigneCommandeDto[]>(`${this.apiServerUrl}/lignecommandes/searchAllLigneCommandeOrderByIdDesc`);
  }

  public getLigneCommandeDtosOrderByIdDesc(): Observable<LigneCommandeDto[]> {
    return this.http.get<LigneCommandeDto[]>(`${this.apiServerUrl}/lignecommandes/findListArticleGroupByIdDesc`);
  }

  public getLigneCommandeDtosByCommandeId(comId: number): Observable<LigneCommandeDto[]> {
    return this.http.get<LigneCommandeDto[]>(`${this.apiServerUrl}/lignecommandes/searchAllLigneCommandesByCommandeId/${comId}`);
  }

  public getLigneCommandeDtoById(lcomId: number): Observable<LigneCommandeDto> {
    return this.http.get<LigneCommandeDto>(`${this.apiServerUrl}/lignecommandes/${lcomId}`);
  }

  public addLigneCommandeDto(lcomDTO: LigneCommandeDto): Observable<LigneCommandeDto> {
    return this.http.post<LigneCommandeDto>(`${this.apiServerUrl}/lignecommandes/create`, lcomDTO);
  }

  public updateLigneCommandeDto(lcomId: number, lcomDTO: LigneCommandeDto): Observable<LigneCommandeDto> {
    return this.http.put<LigneCommandeDto>(`${this.apiServerUrl}/lignecommandes/update/${lcomId}`, lcomDTO);
  }

  public deleteLigneCommandeDto(lcomId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/lignecommandes/delete/${lcomId}`);
  }

}
