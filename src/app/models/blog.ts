import { Utilisateur, UtilisateurDto } from './utilisateur';

export class Blog {
    id: number;
    title: string;
    description: string;
    image: string;
    createdDate: Date;

    utilisateur: Utilisateur;

}

export class BlogDto {
    id: number;
    title: string;
    description: string;
    image: string;
    createdDate: Date;

    utilisateurDto: UtilisateurDto;
}
