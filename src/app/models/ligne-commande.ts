import { CartItem } from './cart-item';
import { Product, ProductDto } from './product';
import { Commande, CommandeDto } from './commande';

export class LigneCommande {
    id: number;
    imageUrl: string;
    numero: number;
    price: number;
    quantity: number;
    productId: number;
    productName: string;
    createdDate: Date;

    commande: Commande;

    product: Product;
    
    constructor(cartItem: CartItem){
        this.imageUrl = cartItem.image;
        this.price = cartItem.unitPrice;
        this.quantity = cartItem.quantity;
        this.productId = cartItem.id;
        this.productName = cartItem.name;
    }

}

export class LigneCommandeDto {
    id: number;
    imageUrl: string;
    numero: number;
    price: number;
    quantity: number;
    productId: number;
    productName: string;

    commandeDto: CommandeDto;

    productDto: ProductDto;
    
    constructor(cartItem: CartItem){
        this.imageUrl = cartItem.image;
        this.price = cartItem.unitPrice;
        this.quantity = cartItem.quantity;
        this.productId = cartItem.id;
    }

}

