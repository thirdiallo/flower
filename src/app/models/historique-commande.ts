import { Commande, CommandeDto } from './commande';

export class HistoriqueCommande {
    id: number;
    createdDate: Date;
    status: string;
    action: string;

    commande: Commande;
}

export class HistoriqueCommandeDto {
    id: number;
    createdDate: Date;
    status: string;
    action: string;

    commandeDto: CommandeDto;
}

