import { Role } from './role';

export class Utilisateur {
    id: number;
    name: string;
    username: string;
    mobile: string;
    email: string;
    password: string;
    photo: string;
    isActive: boolean = false;

    role: Role;

}

export class UtilisateurDto {
    id: number;
    name: string;
    username: string;
    mobile: string;
    email: string;
    password: string;
    photo: string;
    isActive: boolean;

}
