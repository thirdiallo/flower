import { Utilisateur, UtilisateurDto } from './utilisateur';
import { Product, ProductDto } from './product';

export class Wishlist {
    id: number;
    reference: string;
    nombreEtoile: string;
    observation: string;

    product: Product;

    utilisateur: Utilisateur;
  
}

export class WishlistDto {
    id: number;
    reference: string;
    nombreEtoile: string;
    observation: string;

    productDto: ProductDto;

    utilisateurDto: UtilisateurDto;
  
}

